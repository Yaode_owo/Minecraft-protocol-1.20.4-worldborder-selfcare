const mc = require('minecraft-protocol');

const bot = mc.createClient({
  host: 'kaboom.pw',
  port: 25566,
  username: "worldborder",
  version: '1.20.4',
});

bot.position = { x: 0, z: 0 }
let center = { x: 0, z: 0}, size;

bot.on('login', () => {
  console.log('Bot Spawn!') // very basic
  setInterval(() => {
	if (size < 5000) return bot.chat('/worldborder set 10000'); // worldborder size
	if (bot.position.x > center.x + size || bot.position.x < center.x - size || bot.position.z > center.z + size || bot.position.z < center.z - size) bot.chat(`/tp ${center.x} 321 ${center.z}`); // check bot is out of worldborder
	
  }, 1000);
});

bot.on("position", (packet) => {
  bot.position = {
    x: packet.flags & 1 ? this.x + packet.x : packet.x,
    z: packet.flags & 4 ? this.z + packet.z : packet.z,
  }
  bot.write("teleport_confirm", { teleportId: packet.teleportId })
});

bot.on('initialize_world_border', (item) => {
	center = { x: item.x, z: item.z }
	size = item.newDiameter / 2
});

bot.on('world_border_center', (item) => {
	center = { x: item.x, z: item.z }
});

bot.on('world_border_size', (item) => {
	size = item.diameter / 2
});